package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/open")
	public String index() {
		return "Greetings from Spring Boot open API!";
	}

	@RequestMapping("/close")
	public String index2() {
		return "Greetings from Spring Boot close API!";
	}

}
