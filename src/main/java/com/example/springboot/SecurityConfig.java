package com.example.springboot;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


public class SecurityConfig {

    @Configuration
    @EnableWebSecurity
    @Order(1)
    public static class Api extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/close").authorizeRequests()
                    .antMatchers("/close").hasIpAddress("127.0.0.1")
//                    .antMatchers("/close").denyAll();
                    .anyRequest().authenticated();
        }

    }

    @Configuration
    @EnableWebSecurity
    @Order(2)
    public static class Web extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
//            http.antMatcher("/open").authorizeRequests().antMatchers("/open").permitAll();
            http.authorizeRequests().antMatchers("/open").permitAll();
        }
    }
}